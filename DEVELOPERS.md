First things first
------------------

This is a git repository. It follows the branching model described in this blog: <http://nvie.com/posts/a-successful-git-branching-model/>.
Precisely, this repository features a "develop" branch and several "release-x.y" branches and a few feature brunches (i.e. "profiling", cpu-version, etc), besides the "master" branch. As the article in the above blog suggests, development is being done on the "develop" branch. Whenever a release mature version is achieved, it is merged with the "release" and "master" branches and tagged. If you intend to continue developing this code, following this branching model may be very helpful.

Candidate Ciphers
-----------------

When cracking ciphers, if you don't find an immediate match on the endpoints column of a table, you have to recursively apply reduction and keystream generation and check each time if the resulting keystream is a match.

This is exactly what the Candidate Ciphers Kernel does; it calculates what the endpoint of the table would be, given that the cipher would appear in every possible link in the chain. Every resulting possible endpoint is called a Candidate Cipher.

There is one thread running the calculation for each possible position. So the number of total threads in the Candidate Ciphers Kernel equals chain length.

Afterwards, the code checks if one of the Candidate Ciphers is a match in the table's Endpoints. If yes, it recalculates the matching chain up to the occurrence position to get the key.


