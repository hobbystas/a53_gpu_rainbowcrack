Execute GenerateRT_A53_Cuda on linux
------------------------------------

As simple as typing in a console:

./GenerateRT_A53_Cuda --help

The above command will produce the following help message:

Options: 
  -b, --blocks=<n>     number of blocks (required)
  -t, --threads=<n>    number of threads per block (required)
  -i, --tableindex=<n> table index (optional)
  -l, --chainlength=<n> length of each chain (required)
  -c, --numchains=<n>  number of chains in each table (required)
  -s, --seed=<n>       Seed value to initialize LFSR Starting Points Generator (optional)
  -g, --log=<logfile>  logfile to append table generation statistics, such as #blocks and #threads, kernel runtime etc. (optional)
  -d, --display        Display table data on screen instead of saving them in a file (optional)
  --help               Show help

Some of the above command line parameters are required, as stated in brackets:

-c, --numchains=<n>: The number of chains to be generated for a table. This also denotes the number of Starting Points. I've tested this up to 131072. On a decent modern GPU, numbers up to 300000 should be no problem.

-l, --chainlength=<n>: I've tested this up to 200000. Reasonable chain lengths are in the range between 100000 and 300000.

-b, --blocks=<n>, -t, --threads=<n>: These numbers depend on the capability of your GPU. You may test different values depending on the number of chains to be generated and upon some simple calculations. Ideally, for the highest possible utilization of your GPU, you would want this to be something like [ NUMBER OF CHAINS = BLOCKS times THREADS ], since each thread calculates a chain. But this will not always work with the theoretical specifications of your GPU, due to additional usage of other GPU resources (shared & constant memory, used registers, etc.). The GPU will most likely not be able to dedicate the highest number of blocks and threads determined by the specifications of the GPU Architecture. Try different combinations, trim it a bit here and there. If uncertain, set 128 blocks and 64 threads.

About the optional command line parameters:

-i, --tableindex=<n>: This helps in improving the quality of the tables. Reasonable input: any integer in the range of 1-10. (Default 1)
-s, --seed=<n>: Normally you don't need to set a value here, except if you want to reproduce an exact same copy of a table. In this case, choose both times the same seed. Any integer value up to four digits should do the trick.
-g, --log=<logfile>: Here you'll have to specify a logfile name. Non existing files will be created. Existing files will be appended.

Finally, to execute the application set the required command line parameters appropriately.


What's in the tables
--------------------

Sample:

80000164C00000B2;9D2F28C11C1DCF5D42BA91131B66C000
E0000059F000002C;BD016921D9B5A0C02E772528E61FC000
F80000167C00000B;90271E7CE179F93DD8C5E42B3F370000
3E0000051F000002;04EBE435A1F16D3B822270C783A44000
8F800001C7C00000;16D2C22C9B665365F07D87682DA24000

The tables consist of two columns separated by a semicolon (;). The left column holds the Starting Ponts, the right column holds the corresponding End Points. These are binary numbers saved in HEX representation as strings. When loaded in the crack application they are converted from strings to their corresponding binary numbers.
Starting Points represent the key controlling the keystream generator of the A5/3 algorithm and are of 64 bit length. End Points represent the generated keystreams and are of 128 bit length.


Executing the Crack_A53_Cuda
----------------------------

Similarly, type following command in a console to get a help message:

./Crack_A53_Cuda --help


Options: 
  -b, --blocks=<n>     number of blocks for candidate ciphers generation (required)
  -t, --threads=<n>    number of threads per block for candidate ciphers generation (required)
  -i, --tableindex=<n> table index (optional)
  -l, --chainlength=<n> length of each chain (required)
  -c, --numchains=<n>  number of chains in each table (required)
  -p, --cipher=<cipher> Cipher to crack given in HEX notation without "0x" prefix
  -r, --rainbowtable=<rainbow table> rainbow table to use (required)
  -o, --output=<output> file to save results (optional)
  -g, --log=<logfile>  logfile to append table generation statistics, such as #blocks and #threads, kernel runtime etc. (optional)
  -d, --display        Display crack results on screen instead of saving them in a file (optional)
  --help               Show help

As you can see, the crack part has a couple more required parameters: a cipher to crack and a rainbow table.

Any rainbow table generated with the table generation part will work.

A cipher should be an End Point of the table or any intermediate keystream from within a chain.

The "chain length" and the "number of chains" parameters have to be the ones of the table you are using. You can read out this numbers from the table's file name, as it follows a naming convention. Look for the SP-XX and cl-YYYY portions. SP-XX stands for Starting Points, hence number of chains. cl-YYYY stands for Chain length. Use the corresponding XX and YYYY numbers as parameters in the execution command.

The blocks and thread parameters follow the same rules as in the table generation part.

What is "any intermediate keystream from within a chain"?
---------------------------------------------------------

See <https://en.wikipedia.org/wiki/Rainbow_table> to understand how rainbow tables are generated.
........
Now that you got the picture, lets say you want to simulate a successful cipher cracking procedure where the corresponding keystream was produced in some intermediate step of a chain calculation and is therefore not listed as an End Point in your table but should be reproducible.

To do so, try the following procedure:

	(1) 	Create two tables where the chain length for each table will have a different value but the tables are otherwise identical. How to 			do this? Choose a specific seed and index value for both tables, but a different chain length for each table. The number of chains 			to be generated for each table is irrelevant, as long as you pick your cipher from a chain that is included in both tables.
	(2)	Pick an End Point from the table with the shortest chain length as a cipher to crack.
	(3)	Use the table with the longest chain length for cracking the cipher.

How can I rebuild this application from the source code?
--------------------------------------------------------

A prerequisite for this is that you have the CUDA compiler installed which comes with the CUDA Toolkit. You can download the newest version at <https://developer.nvidia.com/cuda-toolkit>.

If you have downloaded the complete git repository of this application, make sure that you are on the "develop" branch:

	git checkout develop

Next, clean any prebuild garbage and rebuild:

	make clean && make

If you want to rebuild the CPU version:

	make -f makefile_cpu clean && make -f makefile_cpu

