*Why?
-----

I developed this application at the Microprocessors and Hardware Lab of the Technical University of Crete <http://www.mhl.tuc.gr>. The primary objective was to get familiar with the NVIDIA CUDA Architecture, explore its abilities and practice on the process of CUDA code optimization for maximum possible utilization and speedup. The use case was chosen just to make the journey a bit more interesting.

*What is it?
------------

An application which generates Rainbow Tables (see <https://en.wikipedia.org/wiki/Rainbow_table> and
<https://infoscience.epfl.ch/record/99512/files/Oech03.pdf>) targeting the GSM A5/3 encryption algorithm
(see <https://www.gsma.com/aboutus/wp-content/uploads/2014/12/a53andgea3specifications.pdf>),
build to run on CUDA enabled NVIDIA GPUs.

*What's in the box?
------------------

The application was developed and build to run on linux OSs. It features a Table Generation part
and a Crack part. Both come with an executable for CUDA enabled GPUs and one for CPU only execution.
The CPU code is a modified derivate of the CUDA code, where the Kernels are replaced by parallel
OpenMP enabled for-loops. The reason for the CPU only builds was to have a means of speedup-gain comparison
between CPU and GPU. OpenMP was used to make this comparison somehow more fair, though besides the use of
OpenMP on the for-loops, no other optimization techniques where applied on the CPU code.

*How does it work?
------------------

Pretty simple:

The GenerateRT_A53_Cuda binary generates the rainbow tables and saves them in a folder alongside the executable.
The tables are saved in .csv file format. (I know, not the best option. I needed to look in the tables frequently
during the development phase and I never made the effort to change this to a binary file format afterwards)

The Crack_A53_Cuda binary uses the generated tables to get the corresponding Key for a single ciphertext
that you provide.

For detailed usage instructions see "MANUAL.md" located in the project's root folder. The code is also thoroughly commented.

*Don't get to excited!
----------------------

There is NO WAY that this application will decrypt real GSM data. This is for two reasons:

(1) The table structure is far to simplified. This paper > <https://www.iacr.org/archive/asiacrypt2005/348/348.pdf>
	is a good starting point to help in understanding how the table structure in the case of streamciphers should be in
	order to make it work with real data.

(2)	This technique falls under the category of a known plaintext attack. To make it work, one would have to
	collect data from an operational GSM Network based on one or more chosen plaintexts. To do this the legal
	way, one would need to have access to a properly shielded room and the necessary equipment to set up a testing GSM
	network and collect the data. What one would be looking for are suitable pairs of plaintext/chiphertext.
	Afterwards, The tables will have to be created using suitable input parameters for the A5/3
	algorithm. These parameters are now "hardcoded" and constant.
	As additional reading material you might check out "Decoding GSM" <https://hyse.org/a51torrents/Decoding-Gsm.pdf>, a study
	targeting the earlier A5/1 GSM encryption algorithm with lots of information on the data collection and analysis part.

But hey, nevertheless, this is a nice piece of software to experiment with.

Enjoy!
