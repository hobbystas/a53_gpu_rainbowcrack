/*
 * A53_GPU_RainbowCrack
 *
 * Copyright (C) 2014-2016 hobbystas <hobbystas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CPU_FUNCTIONS_H_
#define CPU_FUNCTIONS_H_
#include "definitions.h"

// Optimized key extension function: GSM session keys are 64 bit long by design. The Kasumi Block Cipher is controlled by a 128 bit key.
// This function extends the GSM key to 128 bits so that it can be used to control the Kasumi Block Cipher.
#define extendKeyTo128(ck0, ck1, ck2, ck3){	\
	ck2 = ck0;	\
	ck3 = ck1;	\
}

// The encryption function: Uses the keystream produced from kgcore function to encrypt the plaintext "ptext".
// This is used only for testing
/*#define encrypt(pt0, pt1, pt2, pt3, ks0, ks1, ks2, ks3){	\
	ks3 &= 0xC000;		\
\
	ks0 ^= pt0;			\
	ks1 ^= pt1;			\
	ks2 ^= pt2;			\
	ks3 ^= pt3;			\
}
*/


// TODO: The reduction function might need to be checked upon keyspace coverage.
// The Reduction Function being used now: based on the Rainbow Table technique by Philip Oechslin, as described in the www.free-rainbowtables.com forum.
inline void reduceFunction(uint32_t *newSecretKey, uint32_t *currentCipher, uint32_t currentTableIndex, uint32_t chainPosition)
{
	newSecretKey[0] = (currentCipher[0] + currentTableIndex + chainPosition) % (256*256*256);
	newSecretKey[1] = (currentCipher[1] + currentTableIndex + chainPosition) % (256*256*256);
	newSecretKey[2] = newSecretKey[3] = 0;
}

// This facilitates proper assignment of the starting points to the variables used during chain calculation.
inline void LoadRegistersFromGlobalMemory(register128 &currentKey, uint32_t *startingPointsVector, uint32_t chainIndex){

	currentKey.b32[0] = startingPointsVector[(((int)chainIndex) * 2)];
	currentKey.b32[1] = startingPointsVector[(((int)chainIndex) * 2) + 1];

}


// This facilitates proper assignment of the Endpoints to the variables used for the final results.
inline void SaveRegistersToGlobalMemory(register256 &currentEndpoint, uint32_t *endPointsVector, uint32_t chainIndex){

	endPointsVector[(((int)chainIndex) * 4)] = currentEndpoint.b32[0];
	endPointsVector[(((int)chainIndex) * 4) + 1] = currentEndpoint.b32[1];
	endPointsVector[(((int)chainIndex) * 4) + 2] = currentEndpoint.b32[2];
	endPointsVector[(((int)chainIndex) * 4) + 3] = currentEndpoint.b32[3];
}



// This facilitates proper assignment of the cracked keys to the variables used for the final results.
inline void SaveFoundKeysToGlobalMemory(register128 &currentKey, uint32_t *foundKeysVector, uint32_t chainIdx){

	foundKeysVector[chainIdx * 2] = currentKey.b32[0];
	foundKeysVector[(chainIdx * 2) + 1] = currentKey.b32[1];
}


// The decryption function: Uses the chosen plaintext to produce a keystream from the cipher.
inline void decrypt(uint16_t *pt, uint16_t *cipher)
{
	int i;

	for (i = 0; i < 8; i++)
	{
		cipher[i] ^= pt[i];
	}

}

#endif /* CPU_FUNCTIONS_H_ */
