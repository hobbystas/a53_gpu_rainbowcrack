/*
 * A53_GPU_RainbowCrack
 *
 * Copyright (C) 2014-2016 hobbystas <hobbystas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


/* --------------------------------------------------------------------------------------
 *
 * The code in this file is a heavily modified version of the simulation code listed in
 * https://www.gsma.com/aboutus/wp-content/uploads/2014/12/a53andgea3specifications.pdf.
 *
 * ----------------------------------------------------------------------------------- */


/*-----------------------------------------------------------------------
 *						Kasumi.c
 *-----------------------------------------------------------------------
 *
 *	A sample implementation of KASUMI, the core algorithm for the
 *	3GPP Confidentiality and Integrity algorithms.
 *
 *	This has been coded for clarity, not necessarily for efficiency.
 *
 *	This will compile and run correctly on both Intel (little endian)
 *	and Sparc (big endian) machines. (Compilers used supported 32-bit ints).
 *
 *	Version 1.1		08 May 2000
 *
 *-----------------------------------------------------------------------*/


#ifndef CPU_KASUMI_H_
#define CPU_KASUMI_H_
#include "definitions.h"

// Optimized Kasumi sub functions
//TODO: See if it is possible to optimize variable redundancy in F-functions as we did in Kasumi function
#define FI(i, n, s, k){ \
	n = (i>>7);	\
	s = (i&0x7f);	\
	n = S9[n] ^ s;	\
	s = S7[s] ^ (n & 0x7F);	\
	s ^= (k>>9);	\
	n ^= (k&0x1FF);	\
	n = (S9[n] ^ s);	\
	s = (S7[s] ^ (n & 0x7F));	\
	i = (s<<9) + n;	\
}

#define FO(ii, ll, rr, nn, ss, KO1, KO2, KO3, KI1, KI2, KI3){	\
	(ll) = (uint16_t)((ii)>>16);		\
	(rr) = (uint16_t)(ii);				\
\
	(ll) ^= (KO1);						\
	FI((ll), (nn), (ss), (KI1));		\
	(ll) ^= (rr);						\
\
	(rr) ^= (KO2);						\
	FI((rr), (nn), (ss), (KI2));		\
	(rr) ^= (ll);						\
\
	(ll) ^= (KO3);						\
	FI((ll), (nn), (ss), (KI3));		\
	(ll) ^= (rr);						\
\
	(ii) = (((uint32_t)(rr))<<16) + (ll);	\
}

#define FL(in, ll, rr, a, b, KL1, KL2){	\
	(ll) = (uint16_t)((in)>>16);		\
	(rr) = (uint16_t)(in);				\
\
	(a) = ((ll) & (KL1));				\
	(rr) ^= ROL16(a, 1);				\
\
	(b) = ((rr) | (KL2));				\
	(ll) ^= ROL16(b, 1);				\
\
	(in) = (((uint32_t)(ll))<<16) + (rr);	\
}



/*-----------------------------------------------------------------------
 * Kasumi()
 * 			the main algorithm (fig 1). Apply the same pair of operations
 * 			four times. Transforms the 64-bit input.
 * ---------------------------------------------------------------------*/
inline void Kasumi(register64 &data, subKeys &nod, uint16_t *S7, uint16_t *S9)
{
	uint32_t temp;	//, left, right;
	uint16_t l_16, r_16, s9box, s7box, a, b;
	//int n;


	//Start by getting the data into two 32-bit words (endian correct)
	//left = data.b32[0];		right = data.b32[1];

	//n = 0;

// Initial KASUMI do-while loop
/*	do{
		temp = FL( left, n, nod);
		temp = FO( temp, n++, nod);
		right ^= temp;
		temp = FO( right, n, nod);
		temp = FL( temp, n++, nod);
		left ^= temp;
	}while(n <= 7);
*/

// KASUMI loop unrolled
	temp = data.b32[0];
	FL( data.b32[0], l_16, r_16, a, b, nod.KLi1[0], nod.KLi2[0]);
	FO( data.b32[0], l_16, r_16, s9box, s7box, nod.KOi1[0], nod.KOi2[0], nod.KOi3[0], nod.KIi1[0], nod.KIi2[0], nod.KIi3[0]);
	data.b32[1] ^= data.b32[0];
	data.b32[0] = data.b32[1];
	FO( data.b32[0], l_16, r_16, s9box, s7box, nod.KOi1[1], nod.KOi2[1], nod.KOi3[1], nod.KIi1[1], nod.KIi2[1], nod.KIi3[1]);
	FL( data.b32[0], l_16, r_16, a, b, nod.KLi1[1], nod.KLi2[1]);
	data.b32[0] ^= temp;

	temp = data.b32[0];
	FL( data.b32[0], l_16, r_16, a, b, nod.KLi1[2], nod.KLi2[2]);
	FO( data.b32[0], l_16, r_16, s9box, s7box, nod.KOi1[2], nod.KOi2[2], nod.KOi3[2], nod.KIi1[2], nod.KIi2[2], nod.KIi3[2]);
	data.b32[1] ^= data.b32[0];
	data.b32[0] = data.b32[1];
	FO( data.b32[0], l_16, r_16, s9box, s7box, nod.KOi1[3], nod.KOi2[3], nod.KOi3[3], nod.KIi1[3], nod.KIi2[3], nod.KIi3[3]);
	FL( data.b32[0], l_16, r_16, a, b, nod.KLi1[3], nod.KLi2[3]);
	data.b32[0] ^= temp;

	temp = data.b32[0];
	FL( data.b32[0], l_16, r_16, a, b, nod.KLi1[4], nod.KLi2[4]);
	FO( data.b32[0], l_16, r_16, s9box, s7box, nod.KOi1[4], nod.KOi2[4], nod.KOi3[4], nod.KIi1[4], nod.KIi2[4], nod.KIi3[4]);
	data.b32[1] ^= data.b32[0];
	data.b32[0] = data.b32[1];
	FO( data.b32[0], l_16, r_16, s9box, s7box, nod.KOi1[5], nod.KOi2[5], nod.KOi3[5], nod.KIi1[5], nod.KIi2[5], nod.KIi3[5]);
	FL( data.b32[0], l_16, r_16, a, b, nod.KLi1[5], nod.KLi2[5]);
	data.b32[0] ^= temp;

	temp = data.b32[0];
	FL( data.b32[0], l_16, r_16, a, b, nod.KLi1[6], nod.KLi2[6]);
	FO( data.b32[0], l_16, r_16, s9box, s7box, nod.KOi1[6], nod.KOi2[6], nod.KOi3[6], nod.KIi1[6], nod.KIi2[6], nod.KIi3[6]);
	data.b32[1] ^= data.b32[0];
	data.b32[0] = data.b32[1];
	FO( data.b32[0], l_16, r_16, s9box, s7box, nod.KOi1[7], nod.KOi2[7], nod.KOi3[7], nod.KIi1[7], nod.KIi2[7], nod.KIi3[7]);
	FL( data.b32[0], l_16, r_16, a, b, nod.KLi1[7], nod.KLi2[7]);
	data.b32[0] ^= temp;

	//return the correct endian result
	//data.b32[0] = left;		data.b32[1] = right;

}

/*--------------------------------------------------------------------------
 * KeySchedule()
 *
 * 		Build the key schedule. Most "key" operations use 16-bit
 * 		subkeys so we build uint16_t-sized arrays that are "endian" correct.
 * ------------------------------------------------------------------------*/
inline void KeySchedule( uint16_t *k, subKeys &nod, uint16_t *C)
{
	uint16_t Kprime[8];
	//int n;


	//Now build the K'[] keys
	Kprime[0] = (k[0] ^ C[0]);
	Kprime[1] = (k[1] ^ C[1]);
	Kprime[2] = (k[2] ^ C[2]);
	Kprime[3] = (k[3] ^ C[3]);
	Kprime[4] = (k[4] ^ C[4]);
	Kprime[5] = (k[5] ^ C[5]);
	Kprime[6] = (k[6] ^ C[6]);
	Kprime[7] = (k[7] ^ C[7]);

/*	for (n = 0; n < 8; ++n)
	{
		Kprime[n] = (k[n] ^ C[n]);
	}
*/

	//Finally construct the various sub keys

	// Now this method seems to be 200 ms quicker!
	nod.KLi1[0] = ROL16(k[0], 1);
	nod.KLi1[1] = ROL16(k[1], 1);
	nod.KLi1[2] = ROL16(k[2], 1);
	nod.KLi1[3] = ROL16(k[3], 1);
	nod.KLi1[4] = ROL16(k[4], 1);
	nod.KLi1[5] = ROL16(k[5], 1);
	nod.KLi1[6] = ROL16(k[6], 1);
	nod.KLi1[7] = ROL16(k[7], 1);

	nod.KLi2[0] = Kprime[2];
	nod.KLi2[1] = Kprime[3];
	nod.KLi2[2] = Kprime[4];
	nod.KLi2[3] = Kprime[5];
	nod.KLi2[4] = Kprime[6];
	nod.KLi2[5] = Kprime[7];
	nod.KLi2[6] = Kprime[0];
	nod.KLi2[7] = Kprime[1];

	nod.KOi1[0] = ROL16(k[1], 5);
	nod.KOi1[1] = ROL16(k[2], 5);
	nod.KOi1[2] = ROL16(k[3], 5);
	nod.KOi1[3] = ROL16(k[4], 5);
	nod.KOi1[4] = ROL16(k[5], 5);
	nod.KOi1[5] = ROL16(k[6], 5);
	nod.KOi1[6] = ROL16(k[7], 5);
	nod.KOi1[7] = ROL16(k[0], 5);

	nod.KOi2[0] = ROL16(k[5], 8);
	nod.KOi2[1] = ROL16(k[6], 8);
	nod.KOi2[2] = ROL16(k[7], 8);
	nod.KOi2[3] = ROL16(k[0], 8);
	nod.KOi2[4] = ROL16(k[1], 8);
	nod.KOi2[5] = ROL16(k[2], 8);
	nod.KOi2[6] = ROL16(k[3], 8);
	nod.KOi2[7] = ROL16(k[4], 8);

	nod.KOi3[0] = ROL16(k[6], 13);
	nod.KOi3[1] = ROL16(k[7], 13);
	nod.KOi3[2] = ROL16(k[0], 13);
	nod.KOi3[3] = ROL16(k[1], 13);
	nod.KOi3[4] = ROL16(k[2], 13);
	nod.KOi3[5] = ROL16(k[3], 13);
	nod.KOi3[6] = ROL16(k[4], 13);
	nod.KOi3[7] = ROL16(k[5], 13);

	nod.KIi1[0] = Kprime[4];
	nod.KIi1[1] = Kprime[5];
	nod.KIi1[2] = Kprime[6];
	nod.KIi1[3] = Kprime[7];
	nod.KIi1[4] = Kprime[0];
	nod.KIi1[5] = Kprime[1];
	nod.KIi1[6] = Kprime[2];
	nod.KIi1[7] = Kprime[3];

	nod.KIi2[0] = Kprime[3];
	nod.KIi2[1] = Kprime[4];
	nod.KIi2[2] = Kprime[5];
	nod.KIi2[3] = Kprime[6];
	nod.KIi2[4] = Kprime[7];
	nod.KIi2[5] = Kprime[0];
	nod.KIi2[6] = Kprime[1];
	nod.KIi2[7] = Kprime[2];

	nod.KIi3[0] = Kprime[7];
	nod.KIi3[1] = Kprime[0];
	nod.KIi3[2] = Kprime[1];
	nod.KIi3[3] = Kprime[2];
	nod.KIi3[4] = Kprime[3];
	nod.KIi3[5] = Kprime[4];
	nod.KIi3[6] = Kprime[5];
	nod.KIi3[7] = Kprime[6];

/*	for (n = 0; n < 8; n++)
	{
		nod.KLi1[n] = ROL16(k[n], 1);
		nod.KLi2[n] = Kprime[(n+2)&0x7];
		nod.KOi1[n] = ROL16(k[(n+1)&0x7], 5);
		nod.KOi2[n] = ROL16(k[(n+5)&0x7], 8);
		nod.KOi3[n] = ROL16(k[(n+6)&0x7], 13);
		nod.KIi1[n] = Kprime[(n+4)&0x7];
		nod.KIi2[n] = Kprime[(n+3)&0x7];
		nod.KIi3[n] = Kprime[(n+7)&0x7];

	}
*/
}

/*---------------------------------------------------------------------
 *	FI()
 *		The FI function (fig 3).  It includes the S7 and S9 tables.
 *		Transforms a 16-bit value.
 *---------------------------------------------------------------------*/
/*__device__ inline uint16_t FI( uint16_t in, uint16_t subkey )
{
	uint16_t nine, seven;

	//The sixteen bit input is split into two unequal halves,
	//nine bits and seven bits - as is the subkey
	nine = (uint16_t)(in>>7);
	seven = (uint16_t)(in&0x7f);


	//Now run the various operations
	nine = (uint16_t)(S9[nine] ^ seven);
	seven = (uint16_t)(S7[seven] ^ (nine & 0x7F));

	seven ^= (subkey>>9);
	nine ^= (subkey&0x1FF);

	nine = (uint16_t)(S9[nine] ^ seven);
	seven = (uint16_t)(S7[seven] ^ (nine & 0x7F));

	in =(uint16_t)((seven<<9) + nine);

	return(in);
}
*/
/*-------------------------------------------------------------------------
 * FO()
 *
 * 		The FO() function.
 * 		Transforms a 32-bit value. Uses <index> to identify the
 * 		appropriate subkeys to use.
 * ----------------------------------------------------------------------- */
//__device__ inline uint32_t FO(uint32_t in, uint16_t &KOi1, uint16_t &KOi2, uint16_t &KOi3, uint16_t &KIi1, uint16_t &KIi2, uint16_t &KIi3)
/*__device__ inline void FO(uint32_t &in, uint16_t &KOi1, uint16_t &KOi2, uint16_t &KOi3, uint16_t &KIi1, uint16_t &KIi2, uint16_t &KIi3)
{
	uint16_t left, right, seven, nine;

	//Split the input into two 16-bit words
	left = (uint16_t)(in>>16);
	right = (uint16_t) in;


	//Now apply the same basic transformation three times
	left ^= KOi1;
	//left  = FI(left, KIi1);
	FI(left, nine, seven, KIi1);
	left ^= right;

	right ^= KOi2;
	//right  = FI(right, KIi2);
	FI(right, nine, seven, KIi2);
	right ^= left;

	left ^= KOi3;
	//left  = FI(left, KIi3);
	FI(left, nine, seven, KIi3);
	left ^= right;

	//put the two halves back together
	in = (((uint32_t)right)<<16) + left;

	//return(in);

}
*/

/*---------------------------------------------------------------------------
 * FL()
 * 		The FL() function.
 * 		Transforms a 32-bit value. Uses <index> to identify the
 * 		appropriate subkeys to use.
 * -------------------------------------------------------------------------*/

/*
__device__ inline void FL(uint32_t &in, uint16_t &KLi1, uint16_t &KLi2)
{
	uint16_t l, r, a, b;



	//Split out the left and right halves
	l = (uint16_t)(in>>16);
	r = (uint16_t)(in);


	//do th FL() operations
	a = (l & KLi1);
	r ^= ROL16(a, 1);

	b = (r | KLi2);
	l ^= ROL16(b, 1);


	//put the two halves back together
	in = (((uint32_t)l)<<16) + r;

	//return(in);

}
*/

/*-----------------------------------------------------------------------
 *						e n d   o f   K a s u m i . c
 *-----------------------------------------------------------------------*/

#endif /* CPU_KASUMI_H_ */
