/*
 * A53_GPU_RainbowCrack
 *
 * Copyright (C) 2014-2016 hobbystas <hobbystas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CUDA_FUNCTIONS_H_
#define CUDA_FUNCTIONS_H_
#include "definitions.h"

// Optimized key extension function: GSM session keys are 64 bit long by design. The Kasumi Block Cipher is controlled by a 128 bit key.
// This function extends the GSM key to 128 bits so that it can be used to control the Kasumi Block Cipher.
#define extendKeyTo128(ck0, ck1, ck2, ck3){	\
	ck2 = ck0;	\
	ck3 = ck1;	\
}

// The encryption function: Uses the keystream produced from kgcore function to encrypt the plaintext "ptext".
// This is used only for testing
/*#define encrypt(pt0, pt1, pt2, pt3, ks0, ks1, ks2, ks3){	\
	ks3 &= 0xC000;		\ // Masking the keystream to 114 bits. That is the keystream and plaintext size by GSM design.
\
	ks0 ^= pt0;			\
	ks1 ^= pt1;			\
	ks2 ^= pt2;			\
	ks3 ^= pt3;			\
}
*/

// The S-Boxes contain constant values and are read by all threads. Loading them in the shared memory saves resources and makes
// the execution faster.
// This function is used to load the S-Boxes in the shared memory.
__device__ inline void copySboxesToSharedMemory(uint16_t *sharedS7, uint16_t *sharedS9, uint16_t *constS7, uint16_t *constS9)
{
	int i;

	// Copy S7 to shared
	for (i = 0; i < 128; i++)
		sharedS7[i] = constS7[i];

	syncthreads();

	// Copy S9 to shared
	for (i = 0; i < 512; i++)
		sharedS9[i] = constS9[i];

	syncthreads();
}


// Note: The Reduce Function should operate on 16-bit portions to assure endian-correct results!!
// Note - Edit: No need for that, since the calculations for the reduction procedure are taking place on the GPU
// they are being carried out endianness correctly on 32-bit portions as well. I tested it.

// Premature reduce function: Applies Hellman-Style reduction of the 114 bit keystream to a
// 64 bit initial key for the next link in the chain, by keaping the first 64 bits and
// discarding the rest.
__device__ inline void reduceHellmanStyle(uint32_t *newSecretKey, uint32_t *currentCipher)
{
	//memset(&cipher[4], 0, 24);
	newSecretKey[0] = currentCipher[0];
	newSecretKey[1] = currentCipher[1];
	newSecretKey[2] = newSecretKey[3] = 0;
}

// TODO: The reduction function might need to be checked upon keyspace coverage.
// The Reduction Function being used now: based on the Rainbow Table technique by Philip Oechslin, as described in the www.free-rainbowtables.com forum.
__device__ inline void reduceFunction(uint32_t *newSecretKey, uint32_t *currentCipher, uint32_t currentTableIndex, uint32_t chainPosition)
{
	newSecretKey[0] = (currentCipher[0] + currentTableIndex + chainPosition) % (256*256*256);
	newSecretKey[1] = (currentCipher[1] + currentTableIndex + chainPosition) % (256*256*256);
	newSecretKey[2] = newSecretKey[3] = 0;
}


// This moves the Starting Points from the global memory to the registers
__device__ inline void LoadRegistersFromGlobalMemory(register128 &currentKey, uint32_t *startingPointsVector, uint32_t chainIndex){

	currentKey.b32[0] = startingPointsVector[(((int)chainIndex) * 2)];
	currentKey.b32[1] = startingPointsVector[(((int)chainIndex) * 2) + 1];

}

// This is used only for testing
/*__device__ inline void LoadRegistersFromGlobalMemory_blocks(uint32_t &k0, uint32_t &k1, uint32_t *input, uint32_t &numOfChains, uint32_t &chainIndex){

	k0 = input[0 * numOfChains + chainIndex];
	k1 = input[1 * numOfChains + chainIndex];

}
*/

// This moves the Endpoints from the registers to the global memory, so that they can be read out from the cpu, after chain calculation completes.
__device__ inline void SaveRegistersToGlobalMemory(register256 &currentEndpoint, uint32_t *endPointsVector, uint32_t chainIndex){

	endPointsVector[(((int)chainIndex) * 4)] = currentEndpoint.b32[0];
	endPointsVector[(((int)chainIndex) * 4) + 1] = currentEndpoint.b32[1];
	endPointsVector[(((int)chainIndex) * 4) + 2] = currentEndpoint.b32[2];
	endPointsVector[(((int)chainIndex) * 4) + 3] = currentEndpoint.b32[3];
}



// This is used only for testing
/*__device__ inline void SaveRegistersToGlobalMemory_blocks(uint32_t &c0, uint32_t &c1, uint32_t &c2, uint32_t &c3, uint32_t *output, uint32_t &numOfChains, uint32_t &chainIndex){

	output[0 * numOfChains + chainIndex] = c0;
	output[1 * numOfChains + chainIndex] = c1;
	output[2 * numOfChains + chainIndex] = c2;
	output[3 * numOfChains + chainIndex] = c3;
}
*/


// This is used in the crack part to move any cracked keys to the global memory, so that they can be read out from the cpu afterwards.
__device__ inline void SaveFoundKeysToGlobalMemory(register128 &currentKey, uint32_t *foundKeysVector, uint32_t chainIdx){

	foundKeysVector[chainIdx * 2] = currentKey.b32[0];
	foundKeysVector[(chainIdx * 2) + 1] = currentKey.b32[1];
}

// Premature key extention function: Extends the GSM key to 128 bits. Not used any more.
/*__device__ inline void extendKeyTo128(uint32_t *ck)
{
	//cudaMemcpy(&ck[4], &ck[0], 4 * sizeof(uint16_t), cudaMemcpyDeviceToDevice);
	ck[2] = ck[0];
	ck[3] = ck[1];
}
*/

// The encryption function: Uses the keystream produced from kgcore function to encrypt the plaintext "ptext".
// This is used only for testing
__device__ inline void encrypt(uint16_t *pt, uint16_t *keystrm)
{
	int i;

	keystrm[7] &= 0xC000; // Masking the keystream to 114 bits. That is the keystream and plaintext size by GSM design.

	for (i = 0; i < 8; i++)
	{
		keystrm[i] ^= pt[i];
	}

}


#endif /* CUDA_FUNCTIONS_H_ */
