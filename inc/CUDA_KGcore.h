/*
 * A53_GPU_RainbowCrack
 *
 * Copyright (C) 2014-2016 hobbystas <hobbystas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

/* --------------------------------------------------------------------------------------
 *
 * The code in this file is a heavily modified version of the simulation code listed in
 * https://www.gsma.com/aboutus/wp-content/uploads/2014/12/a53andgea3specifications.pdf.
 *
 * ----------------------------------------------------------------------------------- */

#ifndef CUDA_KGCORE_H_
#define CUDA_KGCORE_H_
#include "definitions.h"
#include "CUDA_Kasumi.h"

/*----------------------------------------------------------------
 * 				KGCORE
 * ---------------------------------------------------------------
 *
 * 	A sample implementation of KGCORE, the heart of the
 * 	A5/3 algorithm set.
 *----------------------------------------------------------------*/


/*-----------------------------------------------------------------
 * 	KGcore()
 * 		Given ca, cb, cc, cd, ck, cl generate co
 *-----------------------------------------------------------------*/
__device__ inline void KGcore(uint8_t &ca, uint8_t &cb, uint32_t &cc, uint8_t &cd, uint16_t *ck, uint16_t *co, int cl, uint16_t *S7, uint16_t *S9, uint16_t *constants)
{
	register64 A;			// the modifier
	register64 temp;		// the working register
	uint16_t	ModKey[8];	// Modified Key
	uint32_t	blkcnt;		// The block counter
	int n;
	subKeys nod;


	// Start by building our global modifier

	A.b32[0] = A.b32[1] = 0;
	temp.b32[0] = temp.b32[1] = 0;


	// Initialize register in an endian correct manner:
	// Old way.
	/*A.b8[0]		=	(uint8_t) (cc>>24);
	A.b8[1]		=	(uint8_t) (cc>>16);
	A.b8[2]		=	(uint8_t) (cc>>8);
	A.b8[3]		=	(uint8_t) (cc);
	A.b8[4]		=	(uint8_t) (cb<<3);
	A.b8[4]		|=	(uint8_t) (cd<<2);
	A.b8[5]		=	(uint8_t) (ca);*/

	// New way.
	A.b32[0] = (cc);
	A.b32[1] = (ca<<16);


	// Construct the modified key and then "kasumi" A
	for (n=0; n<8; ++n)
		ModKey[n] = (ck[n] ^ 0x5555);

	KeySchedule(ModKey, nod, constants);

	Kasumi(A, nod, S7, S9);	//First encryption to create modifier


	// Final initialization steps
	blkcnt = 0;

	KeySchedule(ck, nod, constants);

	// Now run the keystream generator. A keystream of length 256 bits will be created. On a real system, 228 bits are needed:
	// 114 bits for encrypting up-link transmission and 114 bits for encrypting down-link transmission. The remaining LSBs would be discarded.
	// In this software, eventually only the first 128 are being kept as intermediate and final Endpoints.
	// KASUMI while block chaining -> while loop unrolled

	// First key stream block
	temp.b32[0] ^= A.b32[0];
	temp.b32[1] ^= A.b32[1];
	temp.b32[1]	^= blkcnt;

	Kasumi(temp, nod, S7, S9);

	*co++ = temp.b32[0]>>16;
	*co++ = temp.b32[0];
	*co++ = temp.b32[1]>>16;
	*co++ = temp.b32[1];

//	co[0] = (temp.b32[0]<<16) + (temp.b32[0]>>16);
//	co[1] = (temp.b32[1]<<16) + (temp.b32[1]>>16);

	cl -= 64;			//Done another 64 bits
	++blkcnt;			//increment BLKCNT

	if(cl <= 0)
		return;

	// Second key stream block
	temp.b32[0] ^= A.b32[0];
	temp.b32[1] ^= A.b32[1];
	temp.b32[1]	^= blkcnt;

	Kasumi(temp, nod, S7, S9);

	*co++ = temp.b32[0]>>16;
	*co++ = temp.b32[0];
	*co++ = temp.b32[1]>>16;
	*co++ = temp.b32[1];

//	co[2] = (temp.b32[0]<<16) + (temp.b32[0]>>16);
//	co[3] = (temp.b32[1]<<16) + (temp.b32[1]>>16);

	cl -= 64;			//Done another 64 bits
	++blkcnt;			//increment BLKCNT

	if(cl <= 0)
		return;

	// Third key stream block
	temp.b32[0] ^= A.b32[0];
	temp.b32[1] ^= A.b32[1];
	temp.b32[1]	^= blkcnt;

	Kasumi(temp, nod, S7, S9);

	*co++ = temp.b32[0]>>16;
	*co++ = temp.b32[0];
	*co++ = temp.b32[1]>>16;
	*co++ = temp.b32[1];

//	co[4] = (temp.b32[0]<<16) + (temp.b32[0]>>16);
//	co[5] = (temp.b32[1]<<16) + (temp.b32[1]>>16);

	cl -= 64;			//Done another 64 bits
	++blkcnt;			//increment BLKCNT

	if(cl <= 0)
		return;

	// Fourth key stream block
	temp.b32[0] ^= A.b32[0];
	temp.b32[1] ^= A.b32[1];
	temp.b32[1]	^= blkcnt;

	Kasumi(temp, nod, S7, S9);

	*co++ = temp.b32[0]>>16;
	*co++ = temp.b32[0];
	*co++ = temp.b32[1]>>16;
	*co++ = temp.b32[1];

//	co[6] = (temp.b32[0]<<16) + (temp.b32[0]>>16);
//	co[7] = (temp.b32[1]<<16) + (temp.b32[1]>>16);

	cl -= 64;			//Done another 64 bits
	++blkcnt;			//increment BLKCNT

	if(cl <= 0)
		return;

// Initial KASUMI while block chaining loop
/*	while(cl > 0)
	{
		//First we calculate the next 64-bits of keystream
		//XOR in A and BLKCNT to last value
		temp.b32[0] ^= A[0];
		temp.b32[1] ^= A[1];
		temp.b32[1]	^= blkcnt;


		//KASUMI it to produce the next block of keystream
		Kasumi(temp.b32, nod);


		//Set <n> to the number of bytes of input data
		//we have to modify .	(=8 if length <=64)
		if(cl >= 64)
			n=2;
		else
			n = (cl+31)/32;


		//Copy out the keystream
		for(i=0; i<n; ++i)
		{
			*co++ = temp.b32[i]>>16;
			*co++ = temp.b32[i];
		}

		cl -= 64;			//Done another 64 bits
		++blkcnt;			//increment BLKCNT
	}*/
}

/*-------------------------------------------------------------------------
 * 				e n d	o f		K G c o r e . c
 * -----------------------------------------------------------------------*/


#endif /* CUDA_KGCORE_H_ */
