/*
 * A53_GPU_RainbowCrack
 *
 * Copyright (C) 2014-2016 hobbystas <hobbystas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CRACK_KERNEL_CPU_H_
#define CRACK_KERNEL_CPU_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <cuda.h>
//#include <cuda_runtime_api.h>
#include "definitions.h"
#include "CPU_Functions.h"
#include "CPU_Kasumi.h"
#include "CPU_KGcore.h"
#include <omp.h>


/* ------------------------ Global Variables Declaration ---------------------------------- */

// Hardcoded A5/3 input parameters: ca, cb, cc, cd, cl. Used as input in the KGCore function.
static int cl = 228;								// Keystream length for both directions
static uint8_t ca = 0x0F , cb = 0 , cd = 0;			// initial parameters
static uint32_t cc = 0x24F20F;						// count

// Kprime creation Constants. Used in KeySchedule for Kasumi Subkeys creation.
static uint16_t C[] = { 0x0123, 0x4567, 0x89AB, 0xCDEF, 0xFEDC, 0xBA98, 0x7654, 0x3210 };

// The 114 bit plaintext. This would be properly set and used when cracking real data. In simulation mode, this is not used and therefore commented out.
//static uint16_t ptext[8] = { 0x0123, 0x4567, 0x89AB, 0xCDEF, 0xEDCB, 0xA987, 0x6543, 0xC000 };

register128 DEVICE_cipher;

// The S-boxes
static uint16_t S7_const[] = {
		54, 50, 62, 56, 22, 34, 94, 96, 38, 6, 63, 93, 2, 18,123, 33,
		55,113, 39,114, 21, 67, 65, 12, 47, 73, 46, 27, 25,111,124, 81,
		53, 9,121, 79, 52, 60, 58, 48,101,127, 40,120,104, 70, 71, 43,
		20,122, 72, 61, 23,109, 13,100, 77, 1, 16, 7, 82, 10,105, 98,
		117,116, 76, 11, 89,106, 0,125,118, 99, 86, 69, 30, 57,126, 87,
		112, 51, 17, 5, 95, 14, 90, 84, 91, 8, 35,103, 32, 97, 28, 66,
		102, 31, 26, 45, 75, 4, 85, 92, 37, 74, 80, 49, 68, 29,115, 44,
		64,107,108, 24,110, 83, 36, 78, 42, 19, 15, 41, 88,119, 59, 3};

static uint16_t S9_const[] = {
		167,239,161,379,391,334,  9,338, 38,226, 48,358,452,385, 90,397,
		183,253,147,331,415,340, 51,362,306,500,262, 82,216,159,356,177,
		175,241,489, 37,206, 17,  0,333, 44,254,378, 58,143,220, 81,400,
		 95,  3,315,245, 54,235,218,405,472,264,172,494,371,290,399, 76,
		165,197,395,121,257,480,423,212,240, 28,462,176,406,507,288,223,
		501,407,249,265, 89,186,221,428,164, 74,440,196,458,421,350,163,
		232,158,134,354, 13,250,491,142,191, 69,193,425,152,227,366,135,
		344,300,276,242,437,320,113,278, 11,243, 87,317, 36, 93,496, 27,
		487,446,482, 41, 68,156,457,131,326,403,339, 20, 39,115,442,124,
		475,384,508, 53,112,170,479,151,126,169, 73,268,279,321,168,364,
		363,292, 46,499,393,327,324, 24,456,267,157,460,488,426,309,229,
		439,506,208,271,349,401,434,236, 16,209,359, 52, 56,120,199,277,
		465,416,252,287,246,  6, 83,305,420,345,153,502, 65, 61,244,282,
		173,222,418, 67,386,368,261,101,476,291,195,430, 49, 79,166,330,
		280,383,373,128,382,408,155,495,367,388,274,107,459,417, 62,454,
		132,225,203,316,234, 14,301, 91,503,286,424,211,347,307,140,374,
		 35,103,125,427, 19,214,453,146,498,314,444,230,256,329,198,285,
		 50,116, 78,410, 10,205,510,171,231, 45,139,467, 29, 86,505, 32,
		 72, 26,342,150,313,490,431,238,411,325,149,473, 40,119,174,355,
		185,233,389, 71,448,273,372, 55,110,178,322, 12,469,392,369,190,
		  1,109,375,137,181, 88, 75,308,260,484, 98,272,370,275,412,111,
		336,318,  4,504,492,259,304, 77,337,435, 21,357,303,332,483, 18,
		 47, 85, 25,497,474,289,100,269,296,478,270,106, 31,104,433, 84,
		414,486,394, 96, 99,154,511,148,413,361,409,255,162,215,302,201,
		266,351,343,144,441,365,108,298,251, 34,182,509,138,210,335,133,
		311,352,328,141,396,346,123,319,450,281,429,228,443,481, 92,404,
		485,422,248,297, 23,213,130,466, 22,217,283, 70,294,360,419,127,
		312,377,  7,468,194,  2,117,295,463,258,224,447,247,187, 80,398,
		284,353,105,390,299,471,470,184, 57,200,348, 63,204,188, 33,451,
		 97, 30,310,219, 94,160,129,493, 64,179,263,102,189,207,114,402,
		438,477,387,122,192, 42,381,  5,145,118,180,449,293,323,136,380,
		 43, 66, 60,455,341,445,202,432, 8,237, 15,376,436,464, 59,461};

/* ------------------------- Kernels Implementation ------------------------------------------- */


void CandidateCiphersGenerationKernel(uint16_t *candidateCiphers, uint32_t tableIndex, int stepsToRun, register128 *DEVICE_cipher){
	// stepsToRun represents chainLength. Candidate Ciphers have to be recreated for all possible positions in the chain,
	// which are as many as chainLength.
	// So, that is how many steps we run and it also denotes the number of threads to be created.

	uint32_t s, chainIdx, currentStep, lastStep;
	register128 newKey;
	register256 newCipher;
	uint32_t *candidateCiphers_32;
	int i;

	candidateCiphers_32 = (uint32_t *)candidateCiphers;
	lastStep = (uint32_t)stepsToRun;
	currentStep = 0;

//	copySboxesToSharedMemory(S7_shared, S9_shared, S7_const, S9_const); // This is Cuda stuff

#pragma omp parallel for

	for(chainIdx = 0; chainIdx < stepsToRun; chainIdx++){

		for(i = 0; i < 8; i++){
			newCipher.b16[i] = DEVICE_cipher->b16[i];
		}

		reduceFunction(newKey.b32, newCipher.b32, tableIndex, currentStep);
		currentStep++;
		//lastStep = (uint32_t)stepsToRun;

		for(s = currentStep; s < lastStep; s++){
			extendKeyTo128(newKey.b32[0], newKey.b32[1], newKey.b32[2], newKey.b32[3]);
			KGcore(ca, cb, cc, cd, newKey.b16, newCipher.b16, cl, S7_const, S9_const, C);
			reduceFunction(newKey.b32, newCipher.b32, tableIndex, s);
		}

		SaveRegistersToGlobalMemory(newCipher, candidateCiphers_32, chainIdx);
	}

}


void RegenerateChainsKernel(uint16_t *startingPoints, uint16_t *foundKeys, uint32_t chainsToRegen, uint32_t chainLength, uint32_t tableIndex, int *reportKey, register128 *DEVICE_cipher){
	register128 newKey;
	register256 newCipher;
	uint32_t chainIdx, chainLenCount;
	int reportReg = 0;
	uint32_t *startingPoints_32, *foundKeys_32;

	startingPoints_32 = (uint32_t *)(startingPoints);
	foundKeys_32 = (uint32_t *)(foundKeys);

//	chainIdx = threadIdx.x + blockIdx.x * blockDim.x; // This is Cuda stuff

//	copySboxesToSharedMemory(S7_shared, S9_shared, S7_const, S9_const); // This is Cuda stuff

#pragma omp parallel for

	for(chainIdx = 0; chainIdx < chainsToRegen; chainIdx++){

		LoadRegistersFromGlobalMemory(newKey, startingPoints_32, chainIdx);

		for(chainLenCount = 0; chainLenCount < chainLength; chainLenCount++){
			extendKeyTo128(newKey.b32[0], newKey.b32[1], newKey.b32[2], newKey.b32[3]);
			KGcore(ca, cb, cc, cd, newKey.b16, newCipher.b16, cl, S7_const, S9_const, C);
			// Check if we have a match and break if we do
			// TODO: Adapt Cryptohaze's way, if the first 32 bits don't match, no need to touch global memory again.
			if(newCipher.b32[0] == DEVICE_cipher->b32[0] && newCipher.b32[1] == DEVICE_cipher->b32[1] &&
					newCipher.b32[2] == DEVICE_cipher->b32[2] && newCipher.b32[3] == DEVICE_cipher->b32[3]){
				reportReg = 1;
				goto return_key;
			}

			reduceFunction(newKey.b32, newCipher.b32, tableIndex, chainLenCount);

		}
		return_key:
		reportKey[chainIdx] = reportReg;
		if(reportReg){
			SaveFoundKeysToGlobalMemory(newKey, foundKeys_32, chainIdx);

	}


		// Used for validation & testing
		//foundKeys[chainIdx].secretKey.b32[0] = newKey.b32[0];
		//foundKeys[chainIdx].secretKey.b32[1] = newKey.b32[1];

		//foundKeys[chainIdx].endPointCipher.b32[0] = newCipher.b32[0];
		//foundKeys[chainIdx].endPointCipher.b32[1] = newCipher.b32[1];
		//foundKeys[chainIdx].endPointCipher.b32[2] = newCipher.b32[2];
		//foundKeys[chainIdx].endPointCipher.b32[3] = newCipher.b32[3];
	}

}


#endif /* CRACK_KERNEL_CPU_H_ */
