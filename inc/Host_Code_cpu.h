/*
 * A53_GPU_RainbowCrack
 *
 * Copyright (C) 2014-2016 hobbystas <hobbystas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef HOST_CODE_CPU_H_
#define HOST_CODE_CPU_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <time.h>
#include "definitions.h"
#include <argtable2.h>



// A function to convert a four character token to a 16 bit Hex number.
void convertCharToHex(char *in, uint16_t *out)
{
	int i;

	char hexbyte[5] = {0};// Converting String to Hexbytes
	for( i = 0; i < strlen(in); i += 4 )
	{
		// Assemble a digit pair into the hexbyte string
		hexbyte[0] = in[i];
		hexbyte[1] = in[i+1];
		hexbyte[2] = in[i+2];
		hexbyte[3] = in[i+3];

		// Convert the hex pair to an integer
		sscanf( hexbyte, "%X", &out[i/4] ) ;
	}
}


// A function that reads out the data from the table file
// and saves them in the tableData structure.
void loadTableDataFromTableFile(FILE *fp, uint16_t *currentKey, uint16_t *currentCipher)
{
	char strbuf[64], *tokbuf;

	fgets(strbuf,sizeof(strbuf),fp);

	tokbuf = strtok(strbuf,";");
	convertCharToHex(tokbuf, currentKey);
	tokbuf = strtok(NULL,"\n");
	convertCharToHex(tokbuf, currentCipher);

}


// The function that saves the calculated Rainbow Table in a .csv file.
void saveData(FILE *fp, int dir, uint16_t *data)
{
	int i;
	if(dir == 0)
	{
		for(i = 0; i < 4; i++)
			fprintf(fp, "%04X",data[i]);
		fprintf(fp, ";");
	}
	else
	{
		for(i = 0; i < 8; i++)
			fprintf(fp, "%04X", data[i]);
		fprintf(fp, "\n");
	}
}

// Table Generation command line parse function
int GenerateTableParseCommandLine(int argc, char *argv[], uint32_t *newTableIndex,
		int *newChainLenght, int *newNumOfChains, uint32_t *newSeed, char *newLogfile, int *log_enable, int *disp_enable){

//    struct arg_int *blocks = arg_int1("b", "blocks", "<n>", "number of blocks (required)");
 //   struct arg_int *threads = arg_int1("t", "threads", "<n>", "number of threads per block (required)");
    struct arg_int *table_index = arg_int0("i", "tableindex", "<n>", "table index (optional)");
    struct arg_int *chain_length = arg_int1("l", "chainlength", "<n>", "length of each chain (required)");
    struct arg_int *num_chains = arg_int1("c", "numchains", "<n>", "number of chains in each table (required)");
    struct arg_int *seed = arg_int0("s", "seed", "<n>", "Seed value to initialize LFSR Starting Points Generator (optional)");
    struct arg_file *log_file = arg_file0("g", "log", "<logfile>", "logfile to append table generation statistics, such as #blocks and #threads, kernel runtime etc. (optional)");
    struct arg_lit *display = arg_lit0("d", "display", "Display table data on screen instead of saving them in a file (optional)");

    struct arg_lit *help = arg_lit0(NULL, "help", "Show help");

    struct arg_end *end = arg_end(20);

    void *argtable[] = {table_index, chain_length, num_chains, seed, log_file, display, help, end};

    // Get arguments, collect data, check for basic errors.
    if (arg_nullcheck(argtable) != 0) {
      printf("error: insufficient memory\n");
    }
    // Look for errors
    int nerrors = arg_parse(argc,argv,argtable);
    if (nerrors > 0) {
      // Print errors, exit.
      arg_print_errors(stdout,end,argv[0]);
      printf("\n\nOptions: \n");
      arg_print_glossary(stdout,argtable,"  %-20s %s\n");
      exit(1);
    }

    if (help->count) {
      printf("\n\nOptions: \n");
      arg_print_glossary(stdout,argtable,"  %-20s %s\n");
      exit(1);
    }

    // Assigning values

//    *newBlocks = *blocks->ival;
//    *newThreads = *threads->ival;

    if (table_index->count){
    	*newTableIndex = (uint32_t) *table_index->ival;
    }else {
		*newTableIndex = (uint32_t) 1;
	}

    *newChainLenght = *chain_length->ival;
    *newNumOfChains = *num_chains->ival;

    if (seed->count){
    	*newSeed = (uint32_t) *seed->ival;
    }else {
		srand(time(NULL));
		*newSeed = (uint32_t) (rand()%901) + 100;//0x01234567; //0x952C4910; //0x81C01F4;
	}

    if (log_file->count){
        strcpy(newLogfile, *log_file->filename);
        *log_enable = 1;
    }else {
		*log_enable = 0;
	}

    if (display->count){
    	*disp_enable = 1;
    }else {
		*disp_enable = 0;
	}

    arg_freetable(argtable,sizeof(argtable)/sizeof(argtable[0]));

    return 0;

}

// Crack cipher command line parse function
int CrackParseCommandLine(int argc, char *argv[], uint32_t *newTableIndex, int *newChainLenght,
		int *newNumOfChains, char *newCipher, char *newTableFile, char *newOutputFile, char *newLogfile, int *out_enable, int *log_enable,
		int *disp_enable, int *ciphers_count){

    struct arg_int *table_index = arg_int0("i", "tableindex", "<n>", "table index (optional)");
    struct arg_int *chain_length = arg_int1("l", "chainlength", "<n>", "length of each chain (required)");
    struct arg_int *num_chains = arg_int1("c", "numchains", "<n>", "number of chains in each table (required)");
    struct arg_str *cipher = arg_str1("p", "cipher", "<cipher>", "Cipher to crack given in HEX notation without \"0x\" prefix");
    struct arg_file *table_file = arg_file1("r", "rainbowtable", "<rainbow table>", "rainbow table to use (required)");
    struct arg_file *output_file = arg_file0("o", "output", "<output>", "file to save results (optional)");
    struct arg_file *log_file = arg_file0("g", "log", "<logfile>", "logfile to append table generation statistics, such as #blocks and #threads, kernel runtime etc. (optional)");
    struct arg_lit *display = arg_lit0("d", "display", "Display crack results on screen instead of saving them in a file (optional)");

    struct arg_lit *help = arg_lit0(NULL, "help", "Show help");

    struct arg_end *end = arg_end(20);

    void *argtable[] = {table_index, chain_length, num_chains, cipher, table_file, output_file, log_file, display, help, end};

    // Get arguments, collect data, check for basic errors.
    if (arg_nullcheck(argtable) != 0) {
      printf("error: insufficient memory\n");
    }
    // Look for errors
    int nerrors = arg_parse(argc,argv,argtable);
    if (nerrors > 0) {
      // Print errors, exit.
      arg_print_errors(stdout,end,argv[0]);
      printf("\n\nOptions: \n");
      arg_print_glossary(stdout,argtable,"  %-20s %s\n");
      exit(1);
    }

    if (help->count) {
      printf("\n\nOptions: \n");
      arg_print_glossary(stdout,argtable,"  %-20s %s\n");
      exit(1);
    }

    // Assigning values


    // TODO: At some point, the index should be read out from the Table Header.
    if (table_index->count){
    	*newTableIndex = (uint32_t) *table_index->ival;
    }else {
		*newTableIndex = (uint32_t) 1;
	}

    *newChainLenght = *chain_length->ival;
    *newNumOfChains = *num_chains->ival;

    *ciphers_count = cipher->count;
    strcpy(newCipher, *cipher->sval);
    strcpy(newTableFile, *table_file->filename);

    if(output_file->count){
    	strcpy(newOutputFile, *output_file->filename);
    	*out_enable = 1;
    }else {
		*out_enable = 0;
	}

    if (log_file->count){
        strcpy(newLogfile, *log_file->filename);
        *log_enable = 1;
    }else {
		*log_enable = 0;
	}


    if (display->count){
    	*disp_enable = 1;
    }else {
		*disp_enable = 0;
	}


    arg_freetable(argtable,sizeof(argtable)/sizeof(argtable[0]));

	return 0;
}


// A function to create a string of date and time information,
// to be used as a file name to save the calculated Rainbow Table.
/*char *filenameFromTime()
{
	char *fileName = NULL;
	time_t rawtime = NULL;

	if((fileName = (char*)malloc(sizeof(char))) == NULL)
	{
		puts("Memory Allocation Error: Rainbow Table File Name!!");
		exit(1);
	}

	time(&rawtime);
	strftime(fileName, 30, "src//%Y.%m.%d_%H:%M:%S.csv", localtime(&rawtime));

	return fileName;
}
*/

// A function to open files for read or write.
/*FILE *openFile(int mod)
{
	FILE *fp;
	//char *fileName;
	if(mod == 0)
	{
		//puts("Please enter the file name containing the Starting Points and press \"enter\"");
		//if((fileName = (char*)malloc(sizeof(char))) == NULL)
		//{
		//	puts("Memory Allocation Error: Input Data File Name!!");
		//	exit(1);
		//}

		//gets(fileName);
		fp = fopen("src/StartingPoints3.csv", "r");

		if (fp==NULL){
			perror ("Error opening file");
			exit(1);
		}

		//free(fileName);
	}
	else
		fp = fopen(filenameFromTime(), "w+");

	return fp;

}
*/

#endif /* HOST_CODE_CPU_H_ */
