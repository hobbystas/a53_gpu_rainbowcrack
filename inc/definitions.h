/*
 * A53_GPU_RainbowCrack
 *
 * Copyright (C) 2014-2016 hobbystas <hobbystas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DEFINITIONS_H_
#define DEFINITIONS_H_

#include <stdint.h>


/*------------- 16 bit rotate left -----------------------------------*/
#define  ROL16(a,b) (uint16_t)((a<<b)|(a>>(16-b)))


/*--------- unions and structures of various bit-lengths to help with endian issues ---------------------*/

typedef union register64{
	uint64_t b64;
	uint32_t b32[2];
	uint16_t b16[4];
	uint8_t  b8[8];
}register64;


typedef union register128{
	uint64_t b64[2];
	uint32_t b32[4];
	uint16_t b16[8];
	uint8_t  b8[16];
}register128;

typedef union register256{
	uint64_t b64[4];
	uint32_t b32[8];
	uint16_t b16[16];
	uint8_t  b8[32];
}register256;


typedef struct chainNode{
	register128 secretKey;
	register256 cipher;
}chainNode;

typedef struct subKeys{
	uint16_t KLi1[8], KLi2[8];
	uint16_t KOi1[8], KOi2[8], KOi3[8];
	uint16_t KIi1[8], KIi2[8], KIi3[8];
}subKeys;


typedef struct tableData{
	register64 secretKey;
	register128 endPointCipher;
}tableData;


#endif /* DEFINITIONS_H_ */
