.SUFFIXES : .cu .cu_dbg.o .c_dbg.o .cpp_dbg.o .cu_rel.o .c_rel.o .cpp_rel.o .cubin .ptx .cu.o .c.o .cpp.o .cuh

###################
# Paths & Folders #
###################

CUDA_INSTALL_PATH = /usr/local/cuda

SRCDIR = ./src
CCSRCDIR = $(SRCDIR)
CPPSRCDIR = $(SRCDIR)
CUSRCDIR = $(SRCDIR)

INCDIR = ./inc
LIBDIR = ./lib


BUILDDIR = ./bin/linux_gpu_cuda
DBGDIR = $(BUILDDIR)/debug
RLSDIR = $(BUILDDIR)/release
OBJDIR = ./obj/linux_gpu_cuda/release


#############
# Compilers #
#############

CC = gcc -fPIC
CXX = g++ -fPIC
NVCC = nvcc 

############
# Includes #
############

INCLUDES  += -I$(CUDA_INSTALL_PATH)/samples/common/inc -I$(CUDA_INSTALL_PATH)/include -I$(CUDA_INSTALL_PATH)/lib64 $(addprefix -I,$(INCDIR))

#################
# Library Paths #
#################

LDFLAGS += -L$(CUDA_INSTALL_PATH)/lib64 -L/usr/local/lib -L/usr/lib -L$(LIBDIR) -largtable2

#############
# Libraries #
#############

#LDLIBS += -largtable2

##############
# Cuda Flags #
##############

#NVCCFLAGS - Debug
#NVCCFLAGS += -G -g -gencode arch=compute_12,code=sm_12 -gencode arch=compute_20,code=sm_20 --ptxas-options=-v -m64 #--library=cuda --cudart static --library-path=/usr/lib/ --library=argtable2 
#CFLAGS += -g -static #-std=c99

#NVCCFLAGS - Release
NVCCFLAGS +=  -O3 --ptxas-options=-v -m64 -lineinfo --library=cuda --cudart static --library-path=$(LIBDIR) --library=argtable2\
 -gencode arch=compute_62,code=sm_62 -gencode arch=compute_61,code=sm_61 -gencode arch=compute_60,code=sm_60\
 -gencode arch=compute_53,code=sm_53 -gencode arch=compute_52,code=sm_52\
 -gencode arch=compute_50,code=sm_50 -gencode arch=compute_37,code=sm_37 -gencode arch=compute_35,code=sm_35 -gencode arch=compute_32,code=sm_32\
 -gencode arch=compute_30,code=sm_30 -gencode arch=compute_20,code=sm_20\
# -gencode arch=compute_12,code=sm_12
CFLAGS +=  -O3 -static #-std=c99

###############
# Input Files #
###############

CUFILES = $(notdir $(foreach dir,$(CUSRCDIR),$(wildcard $(dir)/*.cu)))
CFILES = $(notdir $(foreach dir,$(CCSRCDIR),$(wildcard $(dir)/*.c)))

###########
# Targets #
###########


##########################
# VPATH/vpath directives #
##########################

VPATH = $(CCSRCDIR) $(CPPSRCDIR) $(CUSRCDIR)

################
# Object Files #
################

OBJ_Generate_RT = $(addprefix $(OBJDIR)/,$(addsuffix .o, Generate_RT.c Generate_RT.cu))
OBJ_Crack = $(addprefix $(OBJDIR)/,$(addsuffix .o, Crack.c Crack.cu))

#########
# Rules #
#########

all: $(RLSDIR)/GenerateRT_A53_Cuda $(RLSDIR)/Crack_A53_Cuda

#GRTA53-Test: GRTA53_Test_File.cpp
#	$(NVCC) $^ -G $(INCLUDES) $(LDFLAGS) $(LDLIBS) -c -o $@

$(RLSDIR)/GenerateRT_A53_Cuda: $(OBJ_Generate_RT) #$(LIBDIR)/libargtable2.a
	$(NVCC) $(NVCCFLAGS) $(INCLUDES) $(LDFLAGS) $(LDLIBS)  $^ -o $@
	
$(RLSDIR)/Crack_A53_Cuda: $(OBJ_Crack) #$(LIBDIR)/libargtable2.a
	$(NVCC) $(NVCCFLAGS) $(INCLUDES) $(LDFLAGS) $(LDLIBS) $^ -o $@

	
#%.c.o: %.c
#	$(NVCC) $(INCLUDES) $(CFLAGS) $(LDFLAGS) $(LDLIBS) -c $^ -o $@
#	$(NVCC) -M $< > $@.dep

$(OBJDIR)/%.c.o: %.c
	$(CC) $(INCLUDES) $(CFLAGS) $(LDFLAGS) $(LDLIBS) -c $^ -o $@
#	$(CC) $(INCLUDES) -M $< > $@.dep

$(OBJDIR)/%.cu.o: %.cu
	$(NVCC) $(INCLUDES) $(NVCCFLAGS) $(LDFLAGS) $(LDLIBS) -c $^ -o $@
#	$(NVCC) $(INCLUDES) -M $< > $@.d


clean:
	rm -vrf $(OBJ_Generate_RT) $(OBJ_Crack) $(RLSDIR)/GenerateRT_A53_Cuda $(RLSDIR)/Crack_A53_Cuda


#clean_test:
#	rm GRTA53_Test_File.o GRTA53-Test 

.PHONY : clean
#.PHONY : clean_test
