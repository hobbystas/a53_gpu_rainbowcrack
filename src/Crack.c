/*
 * A53_GPU_RainbowCrack
 *
 * Copyright (C) 2014-2016 hobbystas <hobbystas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <malloc.h>
#include <cuda.h>
#include <cuda_runtime_api.h>
#include "definitions.h"
#include "Host_Code.h"

const char progVer[] = "GPU-Version";


int main(int argc, char *argv[]){

	FILE *Table = NULL, *Output = NULL, *Statistics = NULL;
	int numOfChains, chainLength, ciphersToCrack, candidatesFound = 0, *HOST_reportFoundKeys, *DEVICE_reportFoundKeys, candidateBlocks,
			candidateThreads, outEnable, logEnable, displayEnable, i, j, k;
	tableData *currentTableData;
	char tableFile[1024], outputFile[1024], logfile[1024], cipherString[1024];
	register128 *cipher;
	uint16_t *HOST_candidateCiphers, *DEVICE_candidateCiphers;
	uint32_t tableIndex;
	uint16_t *HOST_chainsToRegen, *DEVICE_chainsToRegen;
	uint16_t *HOST_foundKeys, *DEVICE_foundKeys;
	cudaEvent_t start_timer, stop_timer;
	float kernelRunTime;
	int days, hours, min, sec, msec, x;
	time_t rawtime;
	uint16_t ptext[8] = { 0x0123, 0x4567, 0x89AB, 0xCDEF, 0xEDCB, 0xA987, 0x6543, 0xC000 };



	// Parsing the command line parameters
	CrackParseCommandLine(argc, argv, &candidateBlocks, &candidateThreads, &tableIndex, &chainLength, &numOfChains, cipherString, tableFile, outputFile,
			logfile, &outEnable, &logEnable, &displayEnable, &ciphersToCrack);

	printf("\n\nCracking A5/3 GSM Encryption using Table \"%s\"\n", tableFile);
	puts("=======================================================================================================================\n\n");
	puts(progVer);
	puts("---------------------\n\n");


	puts("Setting up the Timer...\n");
	cudaEventCreate(&start_timer);
	cudaEventCreate(&stop_timer);
	cudaEventRecord(start_timer,0);

	// Create output file
	if(outEnable){
		puts("Opening output file to save crack results ...\n");
		if(!(Output = fopen(outputFile, "a+"))){
			printf("Cannot open file %s to save crack results.\n\nSkipping results file creation\n\n", outputFile);
			return(1);
		}else {
			puts("Output file created successfully!!\n");
		}
		fputs("\n\n==============================================================================================\n\n", Output);
		fprintf(Output, "Cipher to crack: %s\nTable used: %s\n\n", cipherString, tableFile);
	}

	// Allocate Memory
	if(!(HOST_candidateCiphers = (uint16_t *)malloc(sizeof(uint16_t) * chainLength * 8))){
		puts("ERROR: Cannot allocate Candidate Ciphers Host memory!");
		exit(1);
	}

	if(!(cipher = (register128 *)malloc(sizeof(register128) * ciphersToCrack))){
		puts("ERROR: Cannot allocate Cipher Host memory!");
		exit(1);
	}

	if(!(currentTableData = (tableData *)malloc(sizeof(tableData) * numOfChains))){
		puts("ERROR: Cannot allocate Table Data Host memory!");
		exit(1);
	}

	if(cudaErrorMemoryAllocation == cudaMalloc((void **) &DEVICE_candidateCiphers, sizeof(uint16_t) * chainLength * 8)){
		printf("Error: Cannot allocate GPU memory for Candidate Ciphers.\n");
		exit(1);
	}


	cudaMemset(DEVICE_candidateCiphers, 0, sizeof(uint16_t) * chainLength * 8);

	convertCharToHex(cipherString, cipher[0].b16);

	// Hardcoded cipher is used for testing
	//cipher[0].b16[0] = 0xEB95;		cipher[0].b16[1] = 0xDB4C;		cipher[0].b16[2] = 0xCB88;		cipher[0].b16[3] = 0x65C6;
	//cipher[0].b16[4] = 0x6C9D;		cipher[0].b16[5] = 0x5E0F; 		cipher[0].b16[6] = 0xB609;		cipher[0].b16[7] = 0x4000;

	// If we would be operating on real data, the decrypt function should be applied now.
	// Since this software is only a simulation, the decrypt function is commented out.
	// Don't try to apply the decrypt function, unless you know exactly what you are doing.
	//decrypt(ptext, cipher[0].b16);

	copyCipherToGPUConstant(&cipher[0]);


	puts("Candidate Ciphers Generation");
	puts("============================\n");
	puts("Launching Candidate Ciphers Creation Kernel ...\n");

	// See "DEVELOPERS.md" for info on Candidate Ciphers

	LaunchCandidateCiphersKernel(DEVICE_candidateCiphers, candidateBlocks, candidateThreads, tableIndex, chainLength);

	cudaDeviceSynchronize();

	cudaError_t err = cudaGetLastError();

	if( cudaSuccess != err) {
        fprintf(stderr, "Cuda error: %s.\n", cudaGetErrorString(err) );
        exit(1);
    }

	puts("Candidate Ciphers Kernel completed successfully!\n");
	puts("Copying Candidate Ciphers back to Host ...\n");

	cudaMemcpy(HOST_candidateCiphers, DEVICE_candidateCiphers, sizeof(uint16_t) * chainLength * 8, cudaMemcpyDeviceToHost);



	// Print Candidate ciphers on screen if display flag is enabled
	if(displayEnable){
		puts("List of Candidate Ciphers");
		puts("==========================\n");
		for(i = 0; i < chainLength; i++){
			printf("%d: ", i+1);
			for(j = 0; j < 8; j++)
				printf("%04X", HOST_candidateCiphers[(i*8)+j]);
			puts("");
		}
		puts("");
	}

	// Save candidate ciphers to output file
	// This is used for testing
/*	if(Output){

		fputs("Candidate Ciphers\n", Output);
		fputs("-----------------\n", Output);
		for(i = 0; i < chainLength; i++){
			fprintf(Output, "%d: ", i+1);
			for(j = 0; j < 8; j++)
				fprintf(Output, "%04X", HOST_candidateCiphers[(i*8)+j]);
			fputs("\n", Output);
		}
		fputs("\n", Output);
	}
*/
	puts("Opening Table File ...\n");

	if(!(Table = fopen(tableFile, "r"))){
		printf("Cannot open table file %s\n\nAborting Process!!\n\n", tableFile);
		exit(1);
	}else{
		puts("Table Data file opened successfully!!\n");
	}

	puts("Loading Table Data on memory ...\n");

	for(i = 0; i < numOfChains; i++)
	{
		//printf("Chain %d of %d\n\n", i, numOfChains);
		loadTableDataFromTableFile(Table, currentTableData[i].secretKey.b16, currentTableData[i].endPointCipher.b16);
	}

	puts("Table Data loaded successfully!!\n");

	// Print Table Data on screen if display flag is enabled
	// This is used for testing
/*	if(displayEnable){

		puts("Table Data");
		puts("==========\n");
		for(i = 0; i < numOfChains; i++){
			printf("%d: ", i+1);
			for(j = 0; j < 4; j++)
				printf("%04X", currentTableData[i].secretKey.b16[j]);
			printf("->");
			for(j = 0; j < 8; j++)
				printf("%04X", currentTableData[i].endPointCipher.b16[j]);
			puts("");

		}
		puts("");
	}
*/
	fclose(Table);

	puts("Searching for Candidate Ciphers in Table ...\n");

	uint64_t *HOST_candidateCiphers_64;
	HOST_candidateCiphers_64 = (uint64_t *)(HOST_candidateCiphers);

	for(i = 0; i < numOfChains; i++){
		for(k = 0; k < chainLength; k++){
			if(currentTableData[i].endPointCipher.b64[0] == HOST_candidateCiphers_64[k*2] &&
					currentTableData[i].endPointCipher.b64[1] == HOST_candidateCiphers_64[(k*2)+1]){
				HOST_chainsToRegen = realloc(HOST_chainsToRegen, sizeof(uint16_t) * (candidatesFound + 1) * 4);
				for(j = 0; j < 4; j++){
					HOST_chainsToRegen[(candidatesFound*4)+j] = currentTableData[i].secretKey.b16[j];
				}

				if(displayEnable){
					printf("Candidate Cipher %04X%04X%04X%04X%04X%04X%04X%04X  found in chain #%d with starting point %04X%04X%04X%04X in Table \"%s\"\n\n",
							HOST_candidateCiphers[(k*8)], HOST_candidateCiphers[(k*8)+1], HOST_candidateCiphers[(k*8)+2],
							HOST_candidateCiphers[(k*8)+3], HOST_candidateCiphers[(k*8)+4], HOST_candidateCiphers[(k*8)+5],
							HOST_candidateCiphers[(k*8)+6], HOST_candidateCiphers[(k*8)+7], i+1, currentTableData[i].secretKey.b16[0],
							currentTableData[i].secretKey.b16[1], currentTableData[i].secretKey.b16[2], currentTableData[i].secretKey.b16[3],
							tableFile);
				}

				if(Output){
					fprintf(Output, "Candidate Cipher %04X%04X%04X%04X%04X%04X%04X%04X found in chain #%d\nStarting point of chain #%d: %04X%04X%04X%04X\n\n",
							HOST_candidateCiphers[(k*8)], HOST_candidateCiphers[(k*8)+1], HOST_candidateCiphers[(k*8)+2],
							HOST_candidateCiphers[(k*8)+3], HOST_candidateCiphers[(k*8)+4], HOST_candidateCiphers[(k*8)+5],
							HOST_candidateCiphers[(k*8)+6], HOST_candidateCiphers[(k*8)+7], i+1, i+1, currentTableData[i].secretKey.b16[0],
							currentTableData[i].secretKey.b16[1], currentTableData[i].secretKey.b16[2], currentTableData[i].secretKey.b16[3]);

				}
				candidatesFound++;
			}
		}
	}

	// Checking for found Candidates
	if(candidatesFound != 0){

		printf("#%d candidate ciphers have been found in table %s\n\n", candidatesFound, tableFile);

		if(Output){
			fprintf(Output, "#%d candidate ciphers have been found in table %s\n\n", candidatesFound, tableFile);
		}

		puts("Allocating memory on GPU for chains to be regenerated ...\n");

		if(cudaErrorMemoryAllocation == cudaMalloc((void **) &DEVICE_chainsToRegen, sizeof(uint16_t) * candidatesFound * 4)){
			printf("Error: Cannot allocate GPU memory for chains to be regenerated.\n");
			exit(1);
		}

		puts("Allocating Host and GPU memory for found Keys ...\n");

		if(!(HOST_reportFoundKeys = (int *)malloc(sizeof(int) * candidatesFound))){
			puts("ERROR: Cannot allocate Reported found Keys Host memory!");
			exit(1);
		}

		if(!(HOST_foundKeys = (uint16_t *)malloc(sizeof(uint16_t) * candidatesFound * 4))){
			puts("ERROR: Cannot allocate Host memory for found Keys!");
			exit(1);
		}

		if(cudaErrorMemoryAllocation == cudaMalloc((void **) &DEVICE_reportFoundKeys, sizeof(int) * candidatesFound)){
			printf("Error: Cannot allocate GPU memory for Reported Found Keys!\n");
			exit(1);
		}

		cudaMemset(DEVICE_reportFoundKeys, 0, sizeof(int) * candidatesFound);

		if(cudaErrorMemoryAllocation == cudaMalloc((void **) &DEVICE_foundKeys, sizeof(uint16_t) * candidatesFound * 4)){
			printf("Error: Cannot allocate GPU memory for found Keys!\n");
			exit(1);
		}

		cudaMemset(DEVICE_foundKeys, 0, sizeof(uint16_t) * candidatesFound * 4);

		puts("Copying chains to be regenerated to GPU ...\n");

		cudaMemcpy(DEVICE_chainsToRegen, HOST_chainsToRegen, sizeof(uint16_t) * candidatesFound * 4, cudaMemcpyHostToDevice);

		puts("Launching chain regeneration kernel ...\n");

		LaunchRegenerateChainsKernel(DEVICE_chainsToRegen, DEVICE_foundKeys, chainLength, 1, candidatesFound, tableIndex, DEVICE_reportFoundKeys);

		cudaDeviceSynchronize();

		err = cudaGetLastError();

		if( cudaSuccess != err) {
			fprintf(stderr, "Cuda error: %s.\n", cudaGetErrorString(err) );
			exit(1);
		}

		puts("Chain regeneration kernel completed successfully!!\n");
		puts("Copying chain regeneration data back to Host\n");

		cudaMemcpy(HOST_foundKeys, DEVICE_foundKeys, sizeof(uint16_t) * candidatesFound * 4, cudaMemcpyDeviceToHost);

		puts("Checking for found keys ...\n");

		cudaMemcpy(HOST_reportFoundKeys, DEVICE_reportFoundKeys, sizeof(int) * candidatesFound, cudaMemcpyDeviceToHost);

		puts("List of found keys");
		puts("=======================\n");

		if(Output){
			fputs("Found Keys\n", Output);
			fputs("----------\n\n", Output);
		}

		for(k = 0; k < ciphersToCrack; k++){
			for(i = 0; i < candidatesFound; i++){

				if(HOST_reportFoundKeys[i]){

					for(j = 0; j < 4; j++)
						printf("%04X", HOST_foundKeys[(i*4)+j]);

					printf(" -> ");

					for(j = 0; j < 8; j++)
						printf("%04X", cipher[k].b16[j]);

					puts("");

					if(Output){

						for(j = 0; j < 4; j++)
							fprintf(Output, "%04X", HOST_foundKeys[(i*4)+j]);

						fprintf(Output, " -> ");
						for(j = 0; j < 8; j++)
							fprintf(Output, "%04X", cipher[k].b16[j]);

						fputs("\n", Output);
					}
				}
				else {
					puts("No found keys have been reported from GPU\n");
					if(Output)
						fputs("No keys have been found.\n\n", Output);
				}
			}
		}

		if(Output)
			fputs("\n\n", Output);

		free(HOST_chainsToRegen);
		free(HOST_foundKeys);
		free(HOST_reportFoundKeys);
		cudaFree(DEVICE_chainsToRegen);
		cudaFree(DEVICE_foundKeys);
		cudaFree(DEVICE_reportFoundKeys);
	}
	else {
		printf("No candidate ciphers have been found in table: %s\n\n", tableFile);
		if(Output)
			fprintf(Output, "No candidate ciphers have been found in table: %s\n\n", tableFile);
	}

	cudaEventRecord(stop_timer, 0);
	cudaEventSynchronize(stop_timer);
	cudaEventElapsedTime(&kernelRunTime, start_timer, stop_timer);

	msec = (int) kernelRunTime;
	x = msec / 1000;
	sec = x % 60;
	x /= 60;
	min = x % 60;
	x /= 60;
	hours = x % 24;
	x /= 24;
	days = x;


	printf("\nCrack Ciphers Application Runtime:\t%dd %dh %dm and %ds or %f ms\n\n", days, hours, min, sec, kernelRunTime);

	if(Output){
		fputs("==============================================================================================\n\n", Output);
	}

	if(logEnable){
		puts("Saving statistics to logfile ...\n");
		if(!(Statistics = fopen(logfile, "a+"))){
			printf("Cannot open file %s\nSkipping logfile creation.\n\n", logfile);
			return(1);
		}else {
			time(&rawtime);

			fprintf(Statistics, "\n\n\nCracking Statistics for cipher: %s\tcreated on: %s", cipherString, ctime(&rawtime));
			fprintf(Statistics, "=====================================================================================================================\n");
			fprintf(Statistics, "Table used:\t%s\n", tableFile);
			fprintf(Statistics, "Num of Chains:\t\t\t%d\nChain length:\t\t\t%d\nTable Index:\t\t\t%d\nCandidate Blocks:\t\t%d\nCandidate Threads:\t\t%d",
					numOfChains, chainLength, (int)tableIndex, candidateBlocks, candidateThreads);
			fprintf(Statistics, "\nApplication Run Time for Crack Part:\t%dd %dh %dm and %ds or %f ms\n\n", days, hours, min, sec, kernelRunTime);
			printf("Logfile \"%s\" appended successfully with new statistics!\n\n", logfile);

			fclose(Statistics);

		}

	}


	free(HOST_candidateCiphers);
	free(cipher);
	cudaFree(DEVICE_candidateCiphers);
	cudaEventDestroy(start_timer);
	cudaEventDestroy(stop_timer);

	if(Output){
		fclose(Output);
	}


	puts("End of Process\n");

	return 0;

}
