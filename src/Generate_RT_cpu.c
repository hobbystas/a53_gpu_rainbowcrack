/*
 * A53_GPU_RainbowCrack
 *
 * Copyright (C) 2014-2016 hobbystas <hobbystas@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <malloc.h>
#include <time.h>
//#include <cuda.h>
//#include <cuda_runtime_api.h>
//#include <cuda_profiler_api.h>
#include "definitions.h"
#include "Host_Code_cpu.h"
#include "generate_kernel_cpu.h"



//extern "C" void LaunchKernelA53 (chainNode *initialPasswords, int chainLength, u32 CUDA_Blocks, u32 CUDA_Threads);

// General tasks to be accomplished

//TODO: Adapt CUDA_SAFE_CALL
//TODO: Change file structure of RT to binary instead of text
//TODO: Implement a header file for RT
//TODO: Come up with a sorting algorithm for the RT
//TODO: Try out mersine twister as a random starting point generator
//TODO: Add Device Info in log files

const char progVer[] = "CPU-Version";

int main(int argc, char *argv[])
{
	FILE *Table = NULL, *Statistics = NULL;
	uint32_t seed, LFSR, feedback, period, tableIndex;
	int numOfChains, chainLength, lfsrLoop, logEnable, displayEnable, i, j; //,cudaBlocks, cudaThreads
	char filename[1024], logfile[1024];
//	cudaEvent_t start_timer, stop_timer; // This is Cuda stuff
	long kernelRunTime, hours, min, sec, x;
	time_t rawtime, start, stop;
	uint16_t *HOST_Starting_Points_Vector;	//, *DEVICE_Starting_Points_Vector;
	uint16_t *HOST_End_Points_Vector;		//, *DEVICE_End_Points_Vector;
	lfsrLoop = 0;

	// Parsing the command line parameters
	GenerateTableParseCommandLine(argc, argv, &tableIndex, &chainLength, &numOfChains, &seed, logfile, &logEnable, &displayEnable);

	//cudaProfilerStart();

	puts("\n\nRainbow Table Generation for the A5/3 GSM encryption algorithm");
	puts("===================================================================\n\n");
	puts(progVer);
	puts("-----------------------\n\n");


	time(&start);

	// Malloc HOST & DEVICE space
	puts("Allocating Host Memory...\n");

	if(!(HOST_Starting_Points_Vector = (uint16_t *)malloc(sizeof(uint16_t) * (numOfChains * 4)))){
		printf("ERROR: Cannot allocate Starting Points Vector host memory.\n");
		exit(1);
	}

	if(!(HOST_End_Points_Vector = (uint16_t *)malloc(sizeof(uint16_t) * (numOfChains * 8)))){
		printf("ERROR: Cannot allocate End Points Vector host memory.\n");
		exit(1);
	}

	// The rest malloc is cuda stuff
//	if(cudaErrorMemoryAllocation == cudaMalloc((void **) &DEVICE_Starting_Points_Vector, sizeof(uint16_t) * (numOfChains * 4))){
//		printf("Error: Cannot allocate GPU memory for Starting Points Vector.\n");
//		exit(1);
//	}

//	if(cudaErrorMemoryAllocation == cudaMalloc((void **) &DEVICE_End_Points_Vector, sizeof(uint16_t) * (numOfChains * 8))){
//		printf("Error: Cannot allocate GPU memory for End Points Vector.\n");
//		exit(1);
//	}

	// If '1', generate the Starting Points using the LFSR Generator
	printf("Generating Starting Points for %d chains of length %d...\n\n", numOfChains, chainLength);
	if(1){ // Leave if clause hardcoded to '1', except if you know what you are doing!

		// This block implements an LFSR of 32 bits length, used as random numbers generator, guaranteed to not generate duplicates.
		// Each Starting Point should have a 64 bits length. Two consecutive numbers from the LFSR generator make one Starting Point.
		// Staring Points are loaded in the Starting Point vector in 16 bit portions, in order to avoid endian issues when copied to the GPU.

		// Setup LFSR Starting Points Generator
		// Maximum length LFSR taps : 32, 31, 30, 10

		printf("Seed = 0x%08X\n\n", seed);

		LFSR = seed;

		for(i = 0; i < numOfChains; i++){

			feedback  = ((LFSR >> 0) ^ (LFSR >> 1) ^ (LFSR >> 2) ^ (LFSR >> 22) ) & 0x1;
			LFSR =  (LFSR >> 1) | (feedback << 31);
			HOST_Starting_Points_Vector[i*4] = (uint16_t)(LFSR >> 16);
			HOST_Starting_Points_Vector[(i*4)+1] = (uint16_t)LFSR;

			if(LFSR == seed){
				puts("WARNING!! LFSR CLOSED THE LOOP!!\n");
				lfsrLoop = 1;
			}

			feedback  = ((LFSR >> 0) ^ (LFSR >> 1) ^ (LFSR >> 2) ^ (LFSR >> 22) ) & 0x1;
			LFSR =  (LFSR >> 1) | (feedback << 31);
			HOST_Starting_Points_Vector[(i*4)+2] = (uint16_t)(LFSR >> 16);
			HOST_Starting_Points_Vector[(i*4)+3] = (uint16_t)LFSR;

			if(LFSR == seed){
				puts("WARNING!! LFSR CLOSED THE LOOP!!\n");
				lfsrLoop = 1;
			}

		}

	}
	// If '0', insert Starting Points manually for testing
	else{

		HOST_Starting_Points_Vector[0]  = 0x2BD6; HOST_Starting_Points_Vector[1]  = 0x459F; HOST_Starting_Points_Vector[2]  = 0x82C5; HOST_Starting_Points_Vector[3]  = 0xBC00;
		HOST_Starting_Points_Vector[4]  = 0x040E; HOST_Starting_Points_Vector[5]  = 0x00FA;	HOST_Starting_Points_Vector[6]  = 0xE899; HOST_Starting_Points_Vector[7]  = 0x0088;
		HOST_Starting_Points_Vector[8]  = 0xEA3F; HOST_Starting_Points_Vector[9]  = 0x271E;	HOST_Starting_Points_Vector[10]  = 0xE926; HOST_Starting_Points_Vector[11]  = 0x7BBF;
		HOST_Starting_Points_Vector[12]  = 0xD336; HOST_Starting_Points_Vector[13]  = 0xE97C;	HOST_Starting_Points_Vector[14]  = 0x684F; HOST_Starting_Points_Vector[15]  = 0x8859;

	}


//	puts("Setting up the Kernel Timer...\n");
//	cudaEventCreate(&start_timer);
//	cudaEventCreate(&stop_timer);
//	cudaEventRecord(start_timer,0);

	// Copy Device Rainbow Data to GPU
	// This is Cuda stuff
//	puts("Copying Data to GPU...\n");
//	cudaMemcpy(DEVICE_Starting_Points_Vector, HOST_Starting_Points_Vector, sizeof(uint16_t) * (numOfChains * 4), cudaMemcpyHostToDevice);


	// Launching Kernel
	puts("Launching Rainbow Table Generation Kernel now ...\n");
	TableGenerationKernel(HOST_Starting_Points_Vector, HOST_End_Points_Vector, numOfChains, chainLength, tableIndex);

    puts("Kernel completed successfully!\n");


	// If "displayEnable" table data is printed out on screen, else it is saved in a .csv file
	// "displayEnable" flag is set through command line argument
	if(displayEnable){
		int count = 1;
		puts("Rainbow Table");
		puts("=============\n");
		puts("\t\tStarting Point\t\tEnd Point");
		puts("-------------------------------------------------------------------------------\n");

		for(i = 0; i < numOfChains; i++){
			printf("Chain %d of %d:\t", count, numOfChains);
			for(j = 0; j < 4; j++)
				printf("%04X", HOST_Starting_Points_Vector[(i*4)+j]);
			printf("  ->  ");
			for(j = 0; j < 8; j++)
				printf("%04X", HOST_End_Points_Vector[(i*8)+j]);
			puts("");
			count++;
		}
		puts("");
	}else {
		// create a "tables" directory
		mkdir("./tables", S_IRWXO | S_IXOTH | S_IRWXU | S_IRWXG);
		// create a file name
		sprintf(filename, "tables/A53-GSM_idx-%d_SP-%d_cl-%d_sd-0x%0X_%s.csv", tableIndex, numOfChains, chainLength, seed, progVer);
		puts("Saving Rainbow Table to File...\n");

		Table = fopen(filename, "w+");

		if(!Table){
			printf("Cannot open file %s, Aborting Table File creation!\n", filename);
			return(1);
		}

		for(i = 0; i < numOfChains; i++)
		{
			saveData(Table, 0, &HOST_Starting_Points_Vector[i*4]);
			saveData(Table, 1, &HOST_End_Points_Vector[i*8]);
		}

		printf("\nTable file \"%s\" saved successfully!\n\n", filename);
		fclose(Table);
	}

	time(&stop);

	kernelRunTime = (long)(difftime(stop, start));

//	msec = (int) kernelRunTime;
	hours = kernelRunTime / 3600;
	x = kernelRunTime % 3600;
	min = x / 60;
	sec = x % 60;

	if(logEnable){
		puts("Saving statistics to logfile ...\n");
		if(!(Statistics = fopen(logfile, "a+"))){
			printf("Cannot open file %s\nSkipping logfile creation.\n\n", logfile);
			return(1);
		}else {
			time(&rawtime);

			fprintf(Statistics, "\n\n\nTable Generation Statistics for \"%s\"\tcreated on: %s", filename, ctime(&rawtime));
			fprintf(Statistics, "=====================================================================================================================\n");
			fprintf(Statistics, "\nNum of Chains:\t\t%d\nChain length:\t\t%d\nTable Index:\t\t%d\nSeed:\t\t\t%d (0x%08X)",
					numOfChains, chainLength, (int)tableIndex, seed, seed);
			fprintf(Statistics, "\nKernel Run Time:%ldh %ldm and %lds or %ld sec\n\n", hours, min, sec, kernelRunTime);
//			fprintf(Statistics, "\nKernel Run Time in seconds:\t%f sec\n\n", kernelRunTime);
			printf("Logfile \"%s\" appended successfully with new statistics!\n\n", logfile);

			fclose(Statistics);

		}

	}

	if(lfsrLoop !=0)
		puts("WARNING!! LFSR CLOSED THE LOOP!!\n");

	printf("\nKernel Run Time:%ldh %ldm and %lds or %ld sec\n\n", hours, min, sec, kernelRunTime);

	puts("Freeing up allocated memory ...\n");

	free(HOST_Starting_Points_Vector);
	free(HOST_End_Points_Vector);

	// The rest is Cuda stuff
//	cudaFree(DEVICE_Starting_Points_Vector);
//	cudaFree(DEVICE_End_Points_Vector);
//	cudaEventDestroy(start_timer);
//	cudaEventDestroy(stop_timer);

	//cudaProfilerStop();
	//cudaDeviceReset();


	puts("Memory freed successfully!!\n\nEnd Of Process\n");

	return 0;

}

